INTRO
------------------------------------------------------------------------------
This module allows you to "archive" content types by preventing new content
from being created, but still allowing full access to existing nodes.

CONFIGURATION & SETUP
------------------------------------------------------------------------------
Access configuration options at
  yoursite.com/admin/settings/content_type_archive